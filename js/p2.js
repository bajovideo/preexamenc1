function searchCountry() {
    const countryInput = document.getElementById('countryInput');
    const resultContainer = document.getElementById('result');
  
    const countryName = countryInput.value;
  
    if (countryName.trim() === '') {
      alert('Por favor, ingrese un nombre de país válido.');
      return;
    }
  
    fetch(`https://restcountries.com/v3.1/all`)
      .then(response => {
        if (!response.ok) {
          throw new Error('No se pudo obtener la información del país.');
        }
        return response.json();
      })
      .then(data => {
        const country = data.find(country => country.name.common.toLowerCase() === countryName.toLowerCase());
  
        if (!country) {
          throw new Error('País no encontrado.');
        }
  
        const capital = country.capital[0];
        const flagURL = country.flags.png;
  
        const resultHTML = `
          <h2>Información de ${countryName}</h2>
          <p>Capital: ${capital}</p>
          <img id="flag" src="${flagURL}" alt="Bandera de ${countryName}">
        `;
        resultContainer.innerHTML = resultHTML;
      })
      .catch(error => {
        console.error('Error:', error);
        resultContainer.innerHTML = `<p>${error.message}</p>`;
      });
  }
  