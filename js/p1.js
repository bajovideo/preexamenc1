document.getElementById("btnBuscar").addEventListener("click", function () {
    buscarUsuario();
});

function buscarUsuario() {
    var userId = document.getElementById("id").value;

    // Validar que el ID esté en el rango de 1 a 10
    if (!userId || isNaN(userId) || userId < 1 || userId > 10) {
        alert("Por favor, ingrese un ID válido en el rango de 1 a 10.");
        return;
    }

    // Hacer la solicitud a la API
    fetch(`https://jsonplaceholder.typicode.com/users/${userId}`)
        .then(response => response.json())
        .then(data => {
            // Actualizar los campos de entrada con la información obtenida
            document.getElementById("nombre").value = data.name;
            document.getElementById("nombreUsu").value = data.username;
            document.getElementById("email").value = data.email;
            document.getElementById("calle").value = data.address.street;
            document.getElementById("numero").value = data.address.suite;
            document.getElementById("ciudad").value = data.address.city;
        })
        .catch(error => {
            console.error('Error al buscar el usuario:', error);
            alert('No se pudo encontrar el usuario. Verifique el ID y vuelva a intentarlo.');
        });
}
